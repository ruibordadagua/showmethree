//
//  RepTableViewController.swift
//  ShowMeThree
//
//  Created by Rui Bordadagua on 18/02/2020.
//  Copyright © 2020 Rui Bordadagua. All rights reserved.
//

import UIKit

class RepTableViewController: UITableViewController {
    
    var listOfRepos: [String] = []
    
    var paginationList: [String] = []
    
    var currentPage = 0
    let size = 10
          
    override func viewDidLoad() {
        super.viewDidLoad()
        paginationList = createListForPage(page: currentPage)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paginationList.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = paginationList[indexPath.row]
        cell.textLabel?.textColor = .lightGray
        cell.backgroundColor = .clear
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == paginationList.count - 1{
            if currentPage * 10 >= listOfRepos.count{
                return
            }
            currentPage += 1
            let list = createListForPage(page: currentPage)
            paginationList.append(contentsOf: list)
            self.tableView.reloadData()
        }
    }

    // MARK: Helper
    
    private func createListForPage(page: Int) -> [String]{
        let start = page * 10
        let finish = size * (page + 1)
        var list: [String] = []
        for i in start..<finish{
            if i >= listOfRepos.count{
                break
            }
            list.append(listOfRepos[i])
        }
        return list
    }
}
