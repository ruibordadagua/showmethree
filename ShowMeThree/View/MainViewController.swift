//
//  MainViewController.swift
//  ShowMeThree
//
//  Created by Rui Bordadagua on 18/02/2020.
//  Copyright © 2020 Rui Bordadagua. All rights reserved.
//

import UIKit
import Kingfisher

class MainViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var pictureHerePlease: UIImageView!
    
    @IBOutlet weak var avatarTextField: UITextField!
    
    var dataManager: DataManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataManager = DataManager.init()
    }
    
    @IBAction func randomEmoji(_ sender: Any) {
        if let randomEmojiURL = dataManager?.getRandomEmoji()?.url{
            pictureHerePlease.kf.setImage(with: URL(string:randomEmojiURL))
        }
    }
    
    @IBAction func showEmojiFamily(_ sender: Any) {
        if let emojis = dataManager?.getListOfAllEmoji(){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "PictureCollection") as! PictureCollectionViewController
            controller.list = emojis
            controller.dataManager = self.dataManager
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @IBAction func searchForAvatar(_ sender: Any) {
        avatarTextField.resignFirstResponder()
        let text = avatarTextField.text!
        dataManager?.getUser(user: text){ image in
            self.pictureHerePlease.image = image
            self.avatarTextField.text = ""
        }
    }
    
    @IBAction func showMostWantedAvatars(_ sender: Any) {
        if let avatars = dataManager?.getListOfSavedAvatars(){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "PictureCollection") as! PictureCollectionViewController
            controller.list = avatars
            controller.shouldDeleteFromDBUponSelection = true
            controller.dataManager = self.dataManager
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @IBAction func showAppleRepos(_ sender: Any) {
        dataManager?.fetchReposFromGitHub(){ list in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "ReposTableViewController") as! RepTableViewController
            controller.listOfRepos = list
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
}
