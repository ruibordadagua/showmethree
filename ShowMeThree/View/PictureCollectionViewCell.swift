//
//  PictureCollectionViewCell.swift
//  ShowMeThree
//
//  Created by Rui Bordadagua on 18/02/2020.
//  Copyright © 2020 Rui Bordadagua. All rights reserved.
//

import UIKit

class PictureCollectionViewCell: UICollectionViewCell {    
    @IBOutlet weak var picture: UIImageView!
}
