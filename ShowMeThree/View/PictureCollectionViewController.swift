//
//  EmojiCollectionViewController.swift
//  ShowMeThree
//
//  Created by Rui Bordadagua on 18/02/2020.
//  Copyright © 2020 Rui Bordadagua. All rights reserved.
//

import UIKit
import Kingfisher

@objc protocol Picture: class{
    var name: String? { get }
    var url: String? { get }
    @objc optional var image: Data? {get}
}

class PictureCollectionViewController: UICollectionViewController {
    
    var list: [Picture] = []
    
    private let reuseIdentifier = "PictureCell"
    
    var shouldDeleteFromDBUponSelection = false
    
    var dataManager: DataManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        collectionView.refreshControl = refreshControl
    }
    
    // MARK: UICollectionViewDataSource
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return self.list.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! PictureCollectionViewCell
        // Configure the cell
        let picture = self.list[indexPath.row]
        if let imageData = picture.image{
            cell.picture.image = UIImage(data:imageData!)
        }
        else{
            if let imageURL = picture.url{
                cell.picture.kf.setImage(with: URL(string:imageURL))
            }
        }
        return cell
    }
    
    // MARK: UICollectionViewDelegate
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let picture = list[indexPath.row]
        list.remove(at: indexPath.row)
        collectionView.deleteItems(at: [indexPath])
        if shouldDeleteFromDBUponSelection{
            dataManager?.deletePicture(picture: picture)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    @objc func refreshList(refreshControl: UIRefreshControl) {
        if !shouldDeleteFromDBUponSelection {
            if let newList = dataManager?.renewList(currentList: list){
                self.list = newList
            }
            self.collectionView.reloadData()
        }
        refreshControl.endRefreshing()
    }
}
