//
//  DataManager.swift
//  ShowMeThree
//
//  Created by Rui Bordadagua on 18/02/2020.
//  Copyright © 2020 Rui Bordadagua. All rights reserved.
//

import Foundation
import Alamofire

class DataManager{
    
    let dataStorage: DataStorage
    
    init(){
        self.dataStorage = DataStorage()
        let emojis = dataStorage.getListOfEmojis()
        if emojis.count == 0{
            fetchEmojisFromGitHub()
            print("Fetched Emojis")
        }
        print("DataStorage initiated")
    }
    
    // MARK: Network
    
    func fetchReposFromGitHub(completionHandler: @escaping (_ listOfRepos: [String]) -> ()){
        let request = AF.request("https://api.github.com/users/apple/repos")
        request.responseJSON(completionHandler: {data in
            if let responseDictionary = data.value as? [[String:Any]]{
                var list: [String] = []
                for item in responseDictionary{
                    list.append(item["full_name"] as! String)
                }
                completionHandler(list)
            }
        })
    }
    
    func fetchEmojisFromGitHub() {
        let request = AF.request("https://api.github.com/emojis")
        request.responseJSON { (data) in
            print(data)
            if let responseDict = data.value as? [String : String]{
                for item in responseDict{
                    self.dataStorage.saveEmoji(withName: item.key, url: item.value)
                }
            }
        }
    }
    
    func getUser(user: String, completionHandler: @escaping (_ image :UIImage?) -> ()) {
        do{
            if let avatar = dataStorage.getAvatar(withName: user){
                completionHandler(UIImage(data:avatar.image!))
                print("Cached Avatar!")
                return
            }
        }
        var userD = user.folding(options: .diacriticInsensitive, locale: .current)
        userD = userD.trimmingCharacters(in: .whitespaces)

        let url = "https://api.github.com/users/" + userD
        let request = AF.request(url)
        request.responseJSON { (data) in
            if let responseDict = data.value as? [String : Any]{
                if let message = responseDict["message"] as? String{
                    if message == "Not Found"{
                        print("Avatar not found")
                        return
                    }
                }
                let url = responseDict["avatar_url"] as! String
                let id = responseDict["id"] as! NSNumber
                let login = responseDict["login"] as! String
                AF.request(url).responseData { data in
                    self.dataStorage.saveAvatar(login: login, url: url, id: id, image: data.value!)
                    completionHandler(UIImage(data:data.value!))
                }
            }
        }
    }
    
    // MARK: Emoji
    
    func getRandomEmoji() -> Emoji? {
        let emojis = getListOfAllEmoji()
        var randomEmoji:Emoji?
        let count = emojis.count
        if count > 0{
            let random = Int.random(in:0 ... count)
            randomEmoji = emojis[random]
        }
        return randomEmoji
    }
    
    func getListOfAllEmoji() -> [Emoji] {
        let emojis = dataStorage.getListOfEmojis()
        return emojis
    }
    
    // MARK: Avatar
    
    func getAvatar(name: String) -> Avatar?{
        let avatar = dataStorage.getAvatar(withName: name)
        return avatar
    }
    
    func deletePicture(picture: Picture){
        if let avatar = picture as? Avatar{
            dataStorage.removeAvatar(avatar: avatar)
        }
    }
    
    func getListOfSavedAvatars() -> [Avatar] {
        let avatars = dataStorage.getListOfAvatars()
        return avatars
    }
    
    func renewList(currentList: [Picture]) -> [Picture]{
        var list: [Picture] = []
        if  currentList is [Emoji]{
            list = self.getListOfAllEmoji()
        }
        return list
    }
    
}
