//
//  DataStorage.swift
//  ShowMeThree
//
//  Created by Rui Bordadagua on 18/02/2020.
//  Copyright © 2020 Rui Bordadagua. All rights reserved.
//

import Foundation
import CoreData

class DataStorage{
    
    let persistentContainer = NSPersistentContainer(name: "ShowMeThree")
    
    var context: NSManagedObjectContext {
        return self.persistentContainer.viewContext
    }
    
    init(){
        self.persistentContainer.loadPersistentStores { description, error in
            if let error = error {
                print("Could not load store \(error.localizedDescription)")
                return
            }
            print("Store loaded")
        }
    }
    
    // MARK: Avatar functions
    
    func saveAvatar(login: String,url: String, id: NSNumber, image: Data){
        let avatar = Avatar(context: self.context)
        avatar.id = id.int64Value
        avatar.name = login
        avatar.url = url
        avatar.image = image
        self.context.insert(avatar)
        do{
            try self.context.save()
        }catch{
            print("Error saving Avatar")
        }
    }
    
    func removeAvatar(avatar: Avatar){
        self.context.delete(avatar)
        do{
            try context.save()
        }catch{
            print("Error in deleting avatar")
        }
    }
    
    func getAvatar(withName name: String) -> Avatar?{
        var avatar: Avatar?
        let request = NSFetchRequest<Avatar>(entityName: "Avatar")
        request.predicate = NSPredicate(format: "name == %@",name)
        do{
            let avatars = try self.context.fetch(request)
            if avatars.count > 0{
                avatar = avatars[0]
            }
        }catch{
            print("Error obtaining avatar")
        }
        return avatar
    }
    
    func getListOfAvatars() -> [Avatar] {
        var avatars: [Avatar] = []
        do{
            avatars = try self.context.fetch(Avatar.fetchRequest() as NSFetchRequest<Avatar>)
        }catch{
            print("Error obtaining list of avatars")
        }
        return avatars
    }
    
    // MARK: Emoji functions
    
    func saveEmoji(withName name: String,url: String) {
        let emoji = Emoji(context: self.context)
        emoji.name = name
        emoji.url = url
        self.context.insert(emoji)
        do{
            try self.context.save()
        }catch{
            print("Error saving Emoji")
        }
    }
    
    func getListOfEmojis() -> [Emoji] {
        var emojis: [Emoji] = []
        do{
            emojis = try self.context.fetch(Emoji.fetchRequest() as NSFetchRequest<Emoji>)
        }catch{
            print("Error obtaining list of emojis")
        }
        return emojis
    }
    
    func getEmoji(withName name: String) -> Emoji?{
        var emoji: Emoji?
        let request = NSFetchRequest<Emoji>(entityName: "Emoji")
        request.predicate = NSPredicate(format: "name == %@",name)
        do{
            let emojis = try self.context.fetch(request)
            emoji = emojis[0]
        }catch{
            print("Error obtaining emoji")
        }
        return emoji
    }
}
