//
//  Avatar+CoreDataProperties.swift
//  ShowMeThree
//
//  Created by Rui Bordadagua on 18/02/2020.
//  Copyright © 2020 Rui Bordadagua. All rights reserved.
//
//

import Foundation
import CoreData


extension Avatar {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Avatar> {
        return NSFetchRequest<Avatar>(entityName: "Avatar")
    }

    @NSManaged public var name: String?
    @NSManaged public var id: Int64
    @NSManaged public var url: String?
    @NSManaged public var image: Data?

}
